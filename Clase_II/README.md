 * #### Clase III. Python: Variables y control de flujo
	* Introducción a Python: Sintaxis general.
		* [Bionformática en el aula](http://ufq.unq.edu.ar/sbg/files/Guia_Taller_Programacion_Biologia_2019.pdf)
	* Listas, tuplas, sets.
		* [Sintáxis básica](https://bioinf.comav.upv.es/courses/linux/python/listas.html)
		* [Lista anidada](htttp://www.mclibre.org/consultar/python/lecciones/python-listas.html)
		* [Tuplas](https://datacarpentry.org/python-ecology-lesson-es/01-short-introduction-to-Python/index.html)
		* [Sets](https://bioinf.comav.upv.es/courses/linux/python/estructuras_de_datos.html#sets)
	* [Ejercicio_2](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_III/ejercicio_2.md)
	* Control de flujo.
		* [condicionales: if, else, elif](https://bioinf.comav.upv.es/courses/linux/python/control_de_flujo.html)
		* [bucles: for](https://bioinf.comav.upv.es/courses/linux/python/control_de_flujo.html#bucle-for)
		* [bucles: while](https://bioinf.comav.upv.es/courses/linux/python/control_de_flujo.html#while)
	* [Ejercicio_3](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_III/ejercicio_3.md)
	* [Diccionarios](https://bioinf.comav.upv.es/courses/linux/python/estructuras_de_datos.html#)
	* [Ejercicio_4](https://gitlab.com/nmoreyra/python-in-biology/blob/master/Clase_III/ejercicio_4.md)
